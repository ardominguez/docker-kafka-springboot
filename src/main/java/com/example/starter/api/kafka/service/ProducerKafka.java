package com.example.starter.api.kafka.service;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ProducerKafka {

    private static final Logger logger = LoggerFactory.getLogger(KafkaProperties.Producer.class);
    private static final String TOPIC = "users";
    private final KafkaTemplate<String, String> kafkaTemplate;

    public void sendMessage(String msg) {
        logger.info(String.format("#### -> Producing message -> %s", msg));
        this.kafkaTemplate.send(TOPIC, msg);
    }
}