package com.example.starter.api.kafka.controller;

import com.example.starter.api.kafka.model.User;
import com.example.starter.api.kafka.service.ProducerKafka;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/kafka")
public class KafkaController {

    private final ProducerKafka producer;

    @PostMapping(value = "/publish")
    public void sendMessageToKafkaTopic(@RequestParam("msg") String msg) {
        this.producer.sendMessage(msg);
    }
}